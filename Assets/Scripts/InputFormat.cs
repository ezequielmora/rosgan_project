﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputFormat : MonoBehaviour {

    private InputField inputField;

	// Use this for initialization
	void Start () {

        InputField inputField = this.gameObject.GetComponent<InputField>();

        inputField.characterLimit = 4;
        inputField.contentType = InputField.ContentType.DecimalNumber;
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
