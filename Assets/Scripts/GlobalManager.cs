﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;


public class GlobalManager : MonoBehaviour {

    [Header("Diferentes Pantallas de la aplicacion")]
    [Space(5)]
    [Tooltip("Diferentes ventanas que gestionara la app en base a sus rect transform")]

    [SerializeField]
    private RectTransform screen1;

    [SerializeField]
    private RectTransform screen2;

    [SerializeField]
    private RectTransform screen3;

    [SerializeField]
    private RectTransform screen4;

    [SerializeField]
    private RectTransform errorPop;

    [SerializeField]
    private Button startButton;

    [SerializeField]
    private Button sendButton;

    [SerializeField]
    private Button playButton;

    [SerializeField]
    private Button resetButton;

    [SerializeField]
    private Sprite invalidButton;

    [SerializeField]
    private Sprite validButton;

    private bool validationConfirm = false;

    private bool addedChar = false;


    //public bool isEditable = false;
    public bool showCursor = false;


    [Header("Pelicula Institucional de Pantalla Principal")]
    [Space(5)]
    public RawImage movieComponent;
    private MovieTexture movieLoop;
    private AudioSource audioLoop;

    

    [Header("Campos de texto para formulario")]
    [Space(5)]

    [SerializeField]
    private TMPro.TMP_InputField fieldNameAndSurname;

    [SerializeField]
    private TMPro.TMP_InputField fieldCompany;

    [SerializeField]
    private TMPro.TMP_InputField fieldDNI;

    [SerializeField]
    private TMPro.TMP_InputField fieldEmail;

    [SerializeField]
    private TMPro.TMP_InputField fieldPhone;

    [SerializeField]
    private TMPro.TMP_InputField fieldAddress;

    [SerializeField]
    private TMPro.TMP_InputField fieldCity;

    [SerializeField]
    private TMPro.TMP_InputField fieldState;

    [SerializeField]
    private TMPro.TMP_InputField fieldTwitter;

    // Valor a ingresar para el ganado
    [SerializeField]
    private TMPro.TMP_InputField fieldValue;

    // Valor a darle formato con comma usando add o regular expressions
    private string tempValue;

    private System.Diagnostics.Process virtualKeyboard;

    private bool formOpened;

    public static GlobalManager instance;

    private void Awake()
    {
        if(instance == null)
        {

            instance = this;

        }
        else
        {

            Destroy(this);

        }
        DontDestroyOnLoad(this);
    }

    // Use this for initialization
    void Start () {


        //float.Parse(fieldValue.text);

        

        sendButton.GetComponent<Image>().sprite = validButton;
        validationConfirm = false;


        MovieHandler();



        // Logica de eventos para funcionamientos de los botones
        startButton.onClick.AddListener(btn_ClickStart);

        sendButton.onClick.AddListener(btn_ClickSend);

        playButton.onClick.AddListener(btn_ClickPlay);

        resetButton.onClick.AddListener(btn_ClickRestart);


        //Logica de eventos para input field
        //fieldNameAndSurname.GetComponent<TMPro.TMP_InputField>().onValueChanged.AddListener(() => TestVoid());  

        fieldValue.onValueChanged.AddListener(delegate
        {
            //SaveTempValue();
        });


        fieldValue.onEndEdit.AddListener(delegate
        {
            //EndInsertPeriod();
        });


        // Sets the MyValidate method to invoke after the input field's default input validation invoke (default validation happens every time a character is entered into the text field.)
        //fieldValue.onValidateInput += delegate (string input, int charIndex, char addedChar) { return MyValidate(addedChar); };

        //Limite de caracteres para el valor ingresado del ganado
        fieldValue.characterLimit = 5;
        
        
        
        //fieldValue.contentType.
        //fieldEmail.characterValidation.EmailAddress;
		
	}
    // TESIING ON VALIDATE
    /*
    private char MyValidate(char charToValidate)
    {
        //Checks if a dollar sign is entered....
        if (charToValidate == '$')
        {
            // ... if it is change it to an empty character.
            charToValidate = '\0';
        }
        return charToValidate;
    }
    */

    private void Update()
    {
        //Handler que maneja la aparicion/desaparicion del puntero por presion de la tecla espacio
        if (Input.GetKeyDown(KeyCode.Space))
        {

            //isEditable = !isEditable;
            showCursor = !showCursor;

        }

        if(showCursor)
            Cursor.visible = showCursor;

        if (!showCursor)
            Cursor.visible = showCursor;





        if (screen2.gameObject.activeInHierarchy)
        {
            //Debug.Log("SCREEN 2 ACTIVA");

            if ((fieldNameAndSurname.GetComponent<TMP_InputField>().text != "") && (fieldCompany.GetComponent<TMP_InputField>().text != "") && (fieldDNI.GetComponent<TMP_InputField>().text != "") && (fieldEmail.GetComponent<TMP_InputField>().text != "") && (fieldAddress.GetComponent<TMP_InputField>().text != "") && (fieldCity.GetComponent<TMP_InputField>().text != "") && (fieldState.GetComponent<TMP_InputField>().text != ""))
            {

                sendButton.GetComponent<Image>().sprite = validButton;
                validationConfirm = true;
                //Debug.Log("VALID IMAGE");

            } else
            {


                sendButton.GetComponent<Image>().sprite = invalidButton;
                validationConfirm = false;


            }

        }

            /*
            if ( (fieldNameAndSurname.GetComponent<TMP_InputField>().text == "") && (fieldCompany.GetComponent<TMP_InputField>().text == "") && (fieldDNI.GetComponent<TMP_InputField>().text == "") && (fieldEmail.GetComponent<TMP_InputField>().text == "") && (fieldAddress.GetComponent<TMP_InputField>().text == "") && (fieldCity.GetComponent<TMP_InputField>().text == "") && (fieldState.GetComponent<TMP_InputField>().text == ""))
                {
                    validationConfirm = false;
                    Debug.Log("INVALID VALIDATION");

                    
                
                } else if ((fieldPhone.GetComponent<TMP_InputField>().text != "") && (fieldTwitter.GetComponent<TMP_InputField>().text != ""))

                {
                    sendButton.GetComponent<Image>().sprite = validButton;
                    validationConfirm = true;
                    Debug.Log("VALID IMAGE");
                }
            }
            */
            /*
            if (screen3.gameObject.activeInHierarchy)
            {


                tempValue = fieldValue.text.ToString();


                if (fieldValue.text != ".." && addedChar == false)
                {

                    fieldValue.text = tempValue.Insert(2, ",");

                    addedChar = true;
                }


                //if (fieldValue.GetComponent<TMP_InputField>().text == "#")
                //{

                //    fieldValue.GetComponent<TMP_InputField>().text.Insert(1, ".");

                //}


            }
            */

    }

    void MovieHandler()
    {

        movieLoop = movieComponent.GetComponent<RawImage>().texture as MovieTexture;

        audioLoop = movieComponent.GetComponent<AudioSource>();

        audioLoop.clip = movieLoop.audioClip;

        movieLoop.loop = true;

        // Stop movie para que no se tilde
        movieLoop.Stop();

        movieLoop.Play();

        // Stop movie para que no se tilde
        audioLoop.Stop();

        audioLoop.Play();

    }


    void SaveTempValue()
    {

        tempValue = fieldValue.text.ToString();

    }


    void EndInsertPeriod()
    {

        //fieldValueInt = fieldValue.text;

        //fieldValue.text = string.Format("{00:###,#}", 8888);

        //string tempValue = fieldValue.text.ToString();

        if (fieldValue.text != "")
        {
            fieldValue.text = tempValue.Insert(2, ",");
        }
    }




    public bool FormContactInfo
    {
        get
        {
            return formOpened;
        }
        set
        {
            formOpened = value;
            //Debug.Log("ESTADO FORMULARIO" + formOpened);
            //Si esta abierto otro panel no permite abrir el formulario de contacto
            /*
            if (screen1.gameObject.activeSelf==true);
            {
                formOpened = false;
            }
            */
            if (formOpened)
            {
                //movePanel(this.gameObject, ejePosPopUp);

                virtualKeyboard = System.Diagnostics.Process.Start("osk.exe");
                //Debug.Log("KEYBOARD OPENED, PROCESS NUMBER º: " + virtualKeyboard.Id );
        
            }
            else
            {
               
         
                /*
                System.Diagnostics.Process[] running = System.Diagnostics.Process.GetProcesses();
                foreach (System.Diagnostics.Process process in running)
                {
                    //Debug.Log("PROCESS OPENED NAME: " + process.ProcessName + "PROCESS OPENED ID: " + process.Id);
                    try
                    {
                        if (!process.HasExited && process.ProcessName == "osk")
                        {
                            if (process.CloseMainWindow())
                            {
                                process.Close();
                                process.WaitForExit(2000);
                                //Debug.Log("KEYBOARD CLOSED CORRECTLY OSK");
                            }
                        }
                    }
                    catch (System.InvalidOperationException)
                    {
                    }
                }
                */
                if (virtualKeyboard != null)
                {
                    try
                    {
                        virtualKeyboard.Kill();
                    }
                    catch (System.InvalidOperationException)
                    {
                    }

                }
                //Debug.Log("KEYBOARD CLOSED CORRECTLY");
            }
        }
    }

    // TODO: Comportamiento del boton de comenzar la app (screen 1)
    void btn_ClickStart()
    {

        StartApp();
        FormReset();

        //Teclado abre con cambio pantalla
        Invoke("FormOpen", 1.5f);

    }


    void btn_ClickSend()
    {
        //FormSave();

        if(!validationConfirm)
        {
            //if(errorPop != null)
            errorPop.gameObject.SetActive(true);
        }
        // Show UI Warning before proceed if fields are missing
        else
        {
            errorPop.gameObject.SetActive(false);
            FormCloseAndChange();
            KeyboardClose();

            //Teclado abre con cambio pantalla
            Invoke("FormOpen", 1.5f);
        }

    }


    void btn_ClickPlay()
    {
        if (fieldValue.text != "")
        {
            //PopValueSave();
            FormSave();
            PopValueChange();
            KeyboardClose();
        }
    }

    void btn_ClickRestart()
    {

        RestartApp();

    }


    void StartApp()
    {
        screen1.gameObject.SetActive(false);
        screen2.gameObject.SetActive(true);

       

        //TODO: DEFINIR logica de animaciones de ventanas con animator


    }

    public void FormOpen()
    {
        // logica para permita re-abrir el teclado
        KeyboardClose();

        if (!FormContactInfo)
        {
            
            //if (fieldNameAndSurname.onSelect.AddListener) // || fieldCompany.isFocused || fieldDNI.isFocused || fieldEmail.isFocused || fieldPhone.isFocused || fieldAddress.isFocused || fieldCity.isFocused || fieldState.isFocused || fieldTwitter.isFocused || fieldValue.isFocused)
            //{

                FormContactInfo = true;

            //}
            

           // fieldNameAndSurname.onSelect.AddListener(EventOnSelectOpen); // || fieldCompany.isFocused || fieldDNI.isFocused || fieldEmail.isFocused || fieldPhone.isFocused || fieldAddress.isFocused || fieldCity.isFocused || fieldState.isFocused || fieldTwitter.isFocused || fieldValue.isFocused)

        }
        else
        {
            return;
        }


    }

    public void KeyboardClose()
    {
        if (FormContactInfo)
        {
            FormContactInfo = false;
        }

            

    }
    /*
    public void InsertPeriod()
    {

        fieldValue.text = string.Format("{0:#,##0}", 8888);

    }
    */

    void FormSave()
    {
        string fieldNameSurnameText = fieldNameAndSurname.GetComponent<TMP_InputField>().text;
        string fieldCompanyText = fieldCompany.GetComponent<TMP_InputField>().text;
        string fieldDNIText = fieldDNI.GetComponent<TMP_InputField>().text;
        string fieldEmailText = fieldEmail.GetComponent<TMP_InputField>().text;
        string fieldPhoneText = fieldPhone.GetComponent<TMP_InputField>().text;
        string fieldAddressText = fieldAddress.GetComponent<TMP_InputField>().text;
        string fieldCityText = fieldCity.GetComponent<TMP_InputField>().text;
        string fieldStateText = fieldState.GetComponent<TMP_InputField>().text;
        string fieldTwitterText = fieldTwitter.GetComponent<TMP_InputField>().text;
        string fieldValueText = fieldValue.GetComponent<TMP_InputField>().text;


        //TODO: Logica de guardado de datos en TXT
        string textToSave = "\n \r Nombre y Apellido: " + fieldNameSurnameText + " \n \r Empresa: " + fieldCompanyText + "\n \r DNI: " + fieldDNIText + " \n \r Email: " + fieldEmailText + " \n \r Telefono: " + fieldPhoneText + " \n \r Dirección: " + fieldAddressText + " \n \r Ciudad: " + fieldCityText + " \n \r Provincia: " + fieldStateText + " \n \r Twitter: " + fieldTwitterText + " \n \r Valor Ingresado :" + fieldValueText + " \n \n \r";
        //print(textToSave);
        SaveData(textToSave);
    }

    // Formula que resetea los valores de los campos para el ingreso del siguiente participante
    void FormReset()
    {
        // Resetea todos los campos y la validacion de los mismos
        fieldNameAndSurname.GetComponent<TMP_InputField>().text = "";
        fieldCompany.GetComponent<TMP_InputField>().text = "";
        fieldDNI.GetComponent<TMP_InputField>().text = "";
        fieldEmail.GetComponent<TMP_InputField>().text = "";
        fieldPhone.GetComponent<TMP_InputField>().text = "";
        fieldAddress.GetComponent<TMP_InputField>().text = "";
        fieldCity.GetComponent<TMP_InputField>().text = "";
        fieldState.GetComponent<TMP_InputField>().text = "";
        fieldTwitter.GetComponent<TMP_InputField>().text = "";
        fieldValue.GetComponent<TMP_InputField>().text = "";

        sendButton.GetComponent<Image>().sprite = invalidButton;
        validationConfirm = false;


    }

    void FormCloseAndChange()
    {
        screen2.gameObject.SetActive(false);
        screen3.gameObject.SetActive(true);
    }


    void PopValueChange()
    {

        screen3.gameObject.SetActive(false);
        screen4.gameObject.SetActive(true);

        // Cierra el teclado virtual con el bool en el set de FormContactInfo
        FormContactInfo = false;

        //Prepara el reseteo automatico de la app
        StartCoroutine(TimeRestartApp());

    }


    void RestartApp()
    {

        screen4.gameObject.SetActive(false);
        screen1.gameObject.SetActive(true);

        MovieHandler();


        // Reseteo de la aplicacion para que comience de cero
        // Debug.Log("Entro en modo reset de la app");

    }

    private void SaveData(string data)
    {
        try
        {
//#if UNITY_EDITOR
            string archivo = Application.dataPath + "/Informacion.txt";
//#else
//           string archivo = Application.dataPath + "./Informacion.txt";
//#endif
            if (System.IO.File.Exists(archivo))
            {
                using (System.IO.FileStream stream = new System.IO.FileStream(archivo, System.IO.FileMode.Append, System.IO.FileAccess.Write))
                {
                    System.IO.StreamWriter writer = new System.IO.StreamWriter(stream);
                    writer.NewLine = "\n \r";
                    writer.WriteLine(data);
                    writer.Close();
                }
            }
            else
            {
                using (System.IO.StreamWriter writer = System.IO.File.CreateText(archivo))
                {
                    writer.NewLine = "\n \r";
                    writer.WriteLine(data);
                    writer.Close();
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogError("Se produjo una Excepcion: " + e.ToString());
        }

    }

    // Reseteo de la aplicacion dentro de un margen de tiempo sino existe ninguna actividad por parte del usuario
    IEnumerator TimeRestartApp()
    {

        //if(screen2.gameObject.activeSelf == true || screen3.gameObject.activeSelf == true || screen4.gameObject.activeSelf == true)
        if(screen4.gameObject.activeInHierarchy == true)
        {


            yield return new WaitForSeconds(15);

            //screen2.gameObject.SetActive(false);
            //screen3.gameObject.SetActive(false);
            //screen4.gameObject.SetActive(false);

            //screen1.gameObject.SetActive(true);
            //MovieHandler();

            RestartApp();
        }

    }
}
